# linprog_cpp

The repo commes with the ready to go conda environment. You can create anc activate it by:
```
conda env create -f env.yaml
conda activate linprog_cpp
```

To compile the examples run:
```
mkdir build 
cd build
cmake ..
make
```

To run the example:
```
./example
```