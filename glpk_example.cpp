#include <stdio.h>            /* C input/output                       */
#include <stdlib.h>           /* C standard library                   */
#include <glpk.h>             /* GNU GLPK linear/mixed integer solver */

// measuring time 
#include <bits/stdc++.h>

#include <iostream>
#include <Eigen/Dense>

// glpk docs: https://kam.mff.cuni.cz/~elias/glpk.pdf

Eigen::VectorXd dddq_max(7);
Eigen::VectorXd jac(7); // direction.dot(Jac)
Eigen::MatrixXd A_eq(5,7); //V2.T.dot(Jac)
Eigen::VectorXd b_eq(5);

using namespace std;

// add state bounds and equaliti constraints
void build_problem(glp_prob *lp,Eigen::VectorXd ub, Eigen::VectorXd lb, Eigen::VectorXd c,Eigen::MatrixXd A_eq, Eigen::VectorXd b_eq){
  
  int rows = A_eq.rows(), cols = A_eq.cols();
  int ia[1+rows*cols], ja[1+rows*cols];
  double ar[1+rows*cols];

  // set the optimisation direction
  glp_set_obj_dir(lp, GLP_MAX);

  /* fill problem */  
  // argmax c^Tx
  //  s.t.  lb < x < ub
  // setting the double sided bounds on optimisation variables 
  glp_add_cols(lp, cols);
  for(int i=1; i<=7; i++){
    glp_set_col_bnds(lp, i, GLP_DB, lb[i-1], ub[i-1]);
    glp_set_obj_coef(lp, i, c[i-1]);
  }

  // egality constraint build
  // A_eq x = b
  glp_add_rows(lp, rows);
  for(int r=0; r<rows; r++){
    glp_set_row_bnds(lp, r+1, GLP_FX,b_eq[r],b_eq[r]);
    for(int c=0; c<cols; c++){ 
        ia[c+r*cols] = r+1, ja[c+r*cols] = c+1, ar[c+r*cols] = A_eq(r,c); /* a[1,1] = 1 */
    }   
  }
  glp_load_matrix(lp, rows*cols-1, ia, ja, ar);

}

int main(void)
{
  time_t start, end;

  /* Recording the starting clock tick.*/
  start = clock();

  dddq_max << 7500.0,3750.0,5000.0,6250.0,7500.0,10000.0,10000;
  jac << 5.81938436e-01, 2.84954224e-12, 5.81938436e-01, 2.44556955e-12,  7.65961643e-02, 5.65258826e-13, 1.11022302e-16;
  A_eq << -2.04237818e-12, -3.21902001e-01, -4.95040369e-13,  5.90200112e-03, 3.75063891e-13, -7.65965884e-02,  0.00000000e+00,
   0.00000000e+00, -5.81938436e-01,  0.00000000e+00,  4.99438436e-01,  -3.33745248e-13,  1.15438133e-01,  0.00000000e+00,
   0.00000000e+00,  0.00000000e+00,  0.00000000e+00,  0.00000000e+00,   1.00000000e+00,  0.00000000e+00,  2.92365993e-01,
   0.00000000e+00,  1.00000000e+00,  0.00000000e+00, -1.00000000e+00,  -4.89665664e-12, -1.00000000e+00, -9.57932603e-12,
   1.00000000e+00,  4.89663865e-12,  1.00000000e+00,  4.89663865e-12,  -3.67320510e-06,  4.89663865e-12, -9.56306502e-01;
  b_eq << 0,0,0,0,0;

  /* declare variables */
  glp_prob *lp;

  /* create problem */
  lp = glp_create_prob();
  glp_set_prob_name(lp, "short");

  // build problem
  build_problem(lp, dddq_max, -dddq_max, jac, A_eq, b_eq);

  /* solve problem */
  glp_simplex(lp, NULL);
  /* recover and display results */
  double z = glp_get_obj_val(lp);
  printf("dddx = %g\n", z);
  for(int i = 1; i<=7; i++){
    double x = glp_get_col_prim(lp, i);
    printf("dddq_%d = %f\n", i, x);
  }

  /* housekeeping */
  glp_delete_prob(lp);
  glp_free_env();

  // Recording the end clock tick.
  end = clock();
  // Calculating total time taken by the program.
  double time_taken = double(end - start) / double(CLOCKS_PER_SEC);
  cout << "Time taken by program is : " << fixed 
        << time_taken << setprecision(5);
  cout << " sec " << endl;
  return 0;
}