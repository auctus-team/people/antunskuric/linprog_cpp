//* short.c */

#include <stdio.h>            /* C input/output                       */
#include <stdlib.h>           /* C standard library                   */
#include <glpk.h>             /* GNU GLPK linear/mixed integer solver */

int main(void)
{
  /* declare variables */
  glp_prob *lp;
  int ia[1+1000], ja[1+1000];
  double ar[1+1000], z, x[7];
  /* create problem */
  lp = glp_create_prob();
  glp_set_prob_name(lp, "short");
  glp_set_obj_dir(lp, GLP_MAX);
  /* fill problem */
  glp_add_rows(lp, 1);
  glp_set_row_name(lp, 1, "f");
  glp_set_row_bnds(lp, 1, GLP_UP, 0.0, 1.0);
  glp_add_cols(lp, 7);
  for(int i=1; i<=7; i++){
    // glp_set_col_name(lp, 1, "x1");
    glp_set_col_bnds(lp, i, GLP_LO, 0.0, 0.0);
    glp_set_obj_coef(lp, i, 0.6);
    ia[i] = 1, ja[i] = i, ar[i] = 1.0; /* a[1,1] = 1 */
  }
  glp_load_matrix(lp, 7, ia, ja, ar);
  /* solve problem */
  glp_simplex(lp, NULL);
  /* recover and display results */
  z = glp_get_obj_val(lp);
  printf("z = %g\n", z);
  for(int i = 1; i<=7; i++){
    x[i] = glp_get_col_prim(lp, i);
  printf("x%d = %f\n", i, x[i]);
  }
  /* housekeeping */
  glp_delete_prob(lp);
  glp_free_env();
  return 0;
}